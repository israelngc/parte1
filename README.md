# # # PARTE 1

* Crie um diretório com nome de "parte1" e faça esses steps dentro dele.
* Containerize essa aplicação
* Crie um Helm chart contendo todos os componentes necessários para essa aplicação rodar em um cluster de Kubernetes
* Crie uma pipeline com GitLab CI para esse chart ser aplicado em um cluster de Kubernetes (pode usar o minikube)

